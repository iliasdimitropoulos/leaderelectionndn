#include "non-starter.hpp"

#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"

#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"

#include "ns3/random-variable-stream.h"

NS_LOG_COMPONENT_DEFINE("NonStarter");

bool nonStarterIsUp=0;

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(NonStarter);


// register NS-3 type
TypeId
NonStarter::GetTypeId()
{
  static TypeId tid = TypeId("NonStarter").SetParent<ndn::App>().AddConstructor<NonStarter>();
  std::cout << "Hello " << tid << "!\n";
  return tid;
}

// Processing upon start of the application
void
NonStarter::StartApplication()
{
  // initialize ndn::App
  ndn::App::StartApplication();
  std::cout << "Starting Application NonStarter.." << Simulator::Now() << std::endl;

  // Add entry to FIB for `/prefix/sub`
  ndn::FibHelper::AddRoute(GetNode(), "/wake_up", m_face, 0);

}

// Processing when application is stopped
void
NonStarter::StopApplication()
{
  // cleanup ndn::App
  ndn::App::StopApplication();
  std::cout << "Stoping Application..\n";
}

void
NonStarter::SendInterest()
{
  /////////////////////////////////////
  // Sending one Interest packet out //
  /////////////////////////////////////

  // Create and configure ndn::Interest
  std::string prefix="/wake_up/";
  prefix.append(std::to_string(GetNode()->GetId())); // kanw append to id tou kathe komvou giati iparxei thema me to routing twn paketwn. to app den lamvanei interests me akrivws to idio prefix, akoma kai an exei diaforetiko nonce
  auto interest = std::make_shared<ndn::Interest>(prefix);
  Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
  interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
  interest->setInterestLifetime(ndn::time::seconds(1));

  NS_LOG_DEBUG("Sending Interest packet for " << *interest);

  // Call trace (for logging purposes)
  m_transmittedInterests(interest, this, m_face);

  m_appLink->onReceiveInterest(*interest);
  std::cout << "Interest " << *interest << " sent from node " << GetNode() << std::endl;
}

// Callback that will be called when Interest arrives
void
NonStarter::OnInterest(std::shared_ptr<const ndn::Interest> interest)
{
  ndn::App::OnInterest(interest);

  NS_LOG_DEBUG("Received Interest packet for " << interest->getName());
  std::cout << "Interest " << *interest << " was delivered to node " << GetNode() << "." << std::endl;

  const ndn::Name& name = interest->getName();
  nfd::Name::const_iterator pointerToName = name.begin();
  std::string x= "wake_up";
  nfd::Name::Component tmp(x);
std::cout << "Node's " << GetNode() << " nonStarterIsUp=" << nonStarterIsUp << std::endl;
  if ( *pointerToName==tmp && nonStarterIsUp==0) {
   nonStarterIsUp=1;
   SendInterest();
  }

}

// Callback that will be called when Data arrives
void
NonStarter::OnData(std::shared_ptr<const ndn::Data> data)
{
  NS_LOG_DEBUG("Receiving Data packet for " << data->getName());

  std::cout << "DATA received for name " << data->getName() << std::endl;
}

} // namespace ns3