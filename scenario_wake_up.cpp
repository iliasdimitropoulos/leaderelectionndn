
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/point-to-point-module.h"

namespace ns3 {

void createRoutes(Ptr<Node> node_1, Ptr<Node> node_2) {
  ndn::FibHelper fibHelper;
  ndn::FibHelper::AddRoute(node_1, "/wake_up", node_2, 0);
  ndn::FibHelper::AddRoute(node_2, "/wake_up", node_1, 0);
}


int
main(int argc, char* argv[])
{
  
  CommandLine cmd;
  cmd.Parse(argc, argv);

  Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("1Mbps"));
  Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
  Config::SetDefault("ns3::QueueBase::MaxSize", StringValue("20p"));

  NodeContainer nodes;
  nodes.Create(3);

  PointToPointHelper p2p;
  p2p.Install(nodes.Get(0), nodes.Get(1));
  p2p.Install(nodes.Get(1), nodes.Get(2));

  ndn::StackHelper ndnHelper;
  ndnHelper.SetDefaultRoutes(true);
  ndnHelper.InstallAll();

// selecting multicast as a strategy
  ndn::StrategyChoiceHelper::InstallAll("/prefix/sub", "localhost/nfd/strategy/multicast");

// installing apps to nodes
  ndn::AppHelper producerHelper("NonStarter");
  producerHelper.Install(nodes.Get(0));
  producerHelper.Install(nodes.Get(2));
  ndn::AppHelper app1("Starter");
  app1.Install(nodes.Get(1));

  ndn::GlobalRoutingHelper ndnGRH;
  ndnGRH.InstallAll();
  ndnGRH.CalculateRoutes();
  createRoutes(nodes.Get(1), nodes.Get(2));
  createRoutes(nodes.Get(0), nodes.Get(1));

  std::cout << "NonStarterApp: " << nodes.Get(0) << "," << nodes.Get(2) << "	StarterApp: " << nodes.Get(1) << std::endl;

  Simulator::Stop(Seconds(20.0));
  std::cout << "Before Simulator::Run\n";
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}

} 

int
main(int argc, char* argv[])
{
  return ns3::main(argc, argv);
}
